package user

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
)

const CLIENT_ID = "0oarh21dmK8fUrkOF4x6"
const CLIENT_SECRET = "HuxNO19y6GTNF96Nw7OecsjG57YZl_JmS5mhiECX"
const ISSUER = "https://dev-703999.okta.com/oauth2/default"

type Exchange struct {
	Error            string `json:"error,omitempty"`
	ErrorDescription string `json:"error_description,omitempty"`
	AccessToken      string `json:"access_token,omitempty"`
	TokenType        string `json:"token_type,omitempty"`
	ExpiresIn        int    `json:"expires_in,omitempty"`
	Scope            string `json:"scope,omitempty"`
	IdToken          string `json:"id_token,omitempty"`
}

func AddRoutes(routes *gin.Engine) {
	loginRoutes := routes.Group("/api/v1")
	staticLocation := ""
	loginRoutes.GET("/auth", func(c *gin.Context) {
		var redirectPath string
		staticLocation = c.Request.Referer()
		q := c.Request.URL.Query()
		nonce, _ := generateNonce()
		q.Add("client_id", CLIENT_ID)
		q.Add("response_type", "code")
		q.Add("response_mode", "query")
		q.Add("scope", "openid")
		q.Add("grant_types", "authorization_code")
		q.Add("redirect_uri", "http://localhost:9000/api/v1/auth/code")
		q.Add("state", "ApplicationState")
		q.Add("nonce", nonce)
		redirectPath = ISSUER + "/v1/authorize?" + q.Encode()
		c.JSON(http.StatusOK, gin.H{"redirectPath": redirectPath})
	})
	loginRoutes.GET("/auth/code", func(c *gin.Context) {
		code, _ := c.GetQuery("code")
		if code == "" {
			fmt.Println("Unable to get the code from okta server")
			c.JSON(http.StatusServiceUnavailable, gin.H{"message": "Unable to get the code from okta server"})
		}
		state, _ := c.GetQuery("state")
		fmt.Println("code : ", code, ", state : ", state)
		exchange := getToken(code, c)
		c.Redirect(http.StatusMovedPermanently, fmt.Sprintf("%s?token=%s", staticLocation, exchange.AccessToken))
	})
}

func getToken(code string, c *gin.Context) Exchange {
	authHeader := base64.StdEncoding.EncodeToString(
		[]byte(CLIENT_ID + ":" + CLIENT_SECRET))
	q := c.Request.URL.Query()
	q.Add("grant_type", "authorization_code")
	q.Add("code", code)
	q.Add("redirect_uri", "http://localhost:9000/api/v1/auth/code")
	url := ISSUER + "/v1/token?" + q.Encode()
	req, _ := http.NewRequest("POST", url, bytes.NewReader([]byte("")))
	h := req.Header
	h.Add("Authorization", "Basic "+authHeader)
	h.Add("Accept", "application/json")
	h.Add("Content-Type", "application/x-www-form-urlencoded")
	h.Add("Connection", "close")
	h.Add("Content-Length", "0")
	client := &http.Client{}
	resp, _ := client.Do(req)
	body, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	var exchange Exchange
	json.Unmarshal(body, &exchange)
	return exchange
}

func generateNonce() (string, error) {
	nonceBytes := make([]byte, 32)
	_, err := rand.Read(nonceBytes)
	if err != nil {
		return "", fmt.Errorf("could not generate nonce")
	}
	return base64.URLEncoding.EncodeToString(nonceBytes), nil
}
