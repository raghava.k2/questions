package question

import (
	"net/http"
	"questions/http/middleware"
	"questions/model"

	"github.com/gin-gonic/gin"
)

func AddRoutes(routes *gin.Engine) {
	questionRoutes := routes.Group("/api/v1/question", middleware.Authorization())
	questionRoutes.GET("/", func(c *gin.Context) {
		var question = model.Question{}
		c.JSON(http.StatusOK, question.FindAll())
	})
	questionRoutes.POST("/", func(c *gin.Context) {
		var question = model.Question{}
		question.Create("testadasda asdasdada asdasda")
		c.JSON(http.StatusOK, gin.H{})
	})
}
