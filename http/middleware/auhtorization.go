package middleware

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	jwtverifier "github.com/okta/okta-jwt-verifier-golang"
)

const ISSUER = "https://dev-703999.okta.com/oauth2/default"
const CLIENT_ID = "0oarh21dmK8fUrkOF4x6"

func Authorization() gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.Request.Header.Get("Authorization")
		if authHeader == "" {
			c.JSON(http.StatusBadRequest, gin.H{"message": "Authorization token is missing"})
		}
		tokenParts := strings.Split(authHeader, "Bearer ")
		bearerToken := tokenParts[1]
		toValidate := map[string]string{}
		toValidate["aud"] = "api://default"
		toValidate["cid"] = CLIENT_ID
		jwtVerifierSetup := jwtverifier.JwtVerifier{
			Issuer:           ISSUER,
			ClaimsToValidate: toValidate,
		}
		_, err := jwtVerifierSetup.New().VerifyAccessToken(bearerToken)
		if err != nil {
			c.JSON(http.StatusForbidden, gin.H{"message": "Authorization token is not valid"})
		}
		c.Next()
	}
}
