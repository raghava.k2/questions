package main

import (
	"net/http"
	"questions/http/question"
	"questions/http/user"

	"github.com/gin-gonic/gin"
)

var r *gin.Engine

func main() {
	r = gin.Default()
	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "Up and running",
		})
	})
	user.AddRoutes(r)
	question.AddRoutes(r)
	r.Run(":80")
}
