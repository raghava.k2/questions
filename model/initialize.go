package model

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var db *gorm.DB

func init() {
	dsn := "user=raghava password=trust dbname=questions port=5432 sslmode=disable"                                                                     //local DB
	dsn = "user=AdminQuestions@dev-db-web password=SomeThing08 dbname=questions  sslmode=require port=5432 host=dev-db-web.postgres.database.azure.com" //Azure
	tempdb, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect DB")
	}
	db = tempdb
	migrateTables()
}

func migrateTables() {
	db.AutoMigrate(&Question{})
}
