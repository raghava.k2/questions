package model

import (
	"gorm.io/gorm"
)

type Question struct {
	gorm.Model
	Title string
}

func (q *Question) TableName() string {
	return "question"
}

func (q *Question) Create(title string) {
	var question Question = Question{Title: title}
	db.Create(&question)
}

func (q *Question) FindAll() []Question {
	var question []Question
	db.Find(&question)
	return question
}
