FROM golang:latest
WORKDIR /app
COPY . . 
RUN go mod download
ENV GIN_MODE=release
RUN go build main.go
CMD ["./main"]
